O projeto de interface pro tema do Opencart esta sendo desenvolvido no arquivo index.html.
O layout ja esta pré definido e somente necessita de ajuste para telas menores, pois o mesmo é responsivo.
Editar os arquivos (index.html, style.css, bootstrap.css).

Após a customização do layout desenvolver um template php compativel com o open cart. Depois deve se gerar o tema que será instalado
no Opencart

Qualquer dúvida entrar em contato no número (18) 998210225.

Att. Murilo Vergínio dos Santos

Obs: Lembrando que o template do layout é somente para customização e deve preservar os direitos autorais do rodapé e o arquivo Readme
da pasta Readme Layout.